# -*- coding: utf-8 -*-
import re
import os
import time
import logging
import csv

# Selenium Dependencies (install it with pip install selenium)
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException


class RupeReportsCrawler:
    MAIN_URL = "http://rupe.tjmg.jus.br/rupe/justica/publico/certidoes/criarSolicitacaoCertidao.rupe?solicitacaoPublica=true"
    FIRST_INSTANCE_REPORT_TYPES = []
    SECOND_INSTANCE_REPORT_TYPES = []

    def __init__(self, csvFile):

        logging.basicConfig(filename='log.txt', level=logging.INFO)

        self.startBrowser()

        # Load types dynamically
        self.getTypes()

        # Start the scrapping process
        self.readEntitiesFromCSV(csvFile)

    def startBrowser(self, directory=''):
        logging.info('Starting process.')

        chrome_profile = webdriver.ChromeOptions()
        chrome_profile.add_experimental_option("prefs", {
            "download.default_directory": os.path.join(os.getcwd(), 'files', directory),
            "download.prompt_for_download": False,
            "plugins.always_open_pdf_externally": True,
            "download.directory_upgrade": True,
            "plugins.plugins_disabled": ["Chrome PDF Viewer"]
        })

        self.driver = webdriver.Chrome(
            executable_path=os.path.join(os.getcwd(), 'chromedriver'),
            options=chrome_profile
        )

    def getTypes(self):
        driver = self.driver
        driver.get(self.MAIN_URL)
        driver.set_window_size(750, 800)
        driver.implicitly_wait(30)

        reportTypes = Select(driver.find_element(By.NAME, 'formCriacaoSolicitacaoCertidao:tipoCertidaoCertidao'))
        for type in reportTypes.options:
            if(type.get_attribute('value') != ''):
            	self.FIRST_INSTANCE_REPORT_TYPES.append(type.get_attribute('value'))

        driver.find_element_by_id('formCriacaoSolicitacaoCertidao:tipoInstanciaCertidao:1').click()
        time.sleep(1)

        reportTypes = Select(driver.find_element(By.NAME, 'formCriacaoSolicitacaoCertidao:tipoCertidaoCertidao'))
        for type in reportTypes.options:
            if(type.get_attribute('value') != ''):
                self.SECOND_INSTANCE_REPORT_TYPES.append(type.get_attribute('value'))

        driver.quit()
        # Uncomment the following lines to display all report types.
        # You can use this to set them manually and disable this function to filter which reports you want.
    def readEntitiesFromCSV(self, file):
        with open(file) as file:
            rows = csv.DictReader(file)

             # For each rows of the file, will execute the following
            for row in rows:
                logging.info('Gathering reports for %s' % (row['nome']))
                self.startBrowser(row['nome'])

                # And inside that entity (either person or company), will grab all types
                for type in self.FIRST_INSTANCE_REPORT_TYPES:

                    # Skips because this report is only available for companies.
                    if(row['tipopessoa'] == 'fisica' and type == 'FALENCIA_CONCORDATA'):
                        continue

                    if(row['tipopessoa'] == 'juridica' and (type == 'INSOLVENCIA' or type == 'TUTELA_CURATELA')):
                        continue

                    self.getReport(row, 1, type)

                # The same applies to second instance reports
                for type in self.SECOND_INSTANCE_REPORT_TYPES:
                    self.getReport(row, 2, type)

                self.driver.quit()

    def getReport(self, row, instance, type):
        driver = self.driver
        driver.get(self.MAIN_URL)
        driver.set_window_size(750, 800)
        driver.implicitly_wait(30)

        # That's just a hack that I had to inject so we can handle the "enter" keypress
        driver.execute_script('document.onkeydown=function(e){e=e||window.event;isEnter="key"in e?"Enter"==e.key:13==e.keyCode,isEnter&&document.getElementById("formCriacaoSolicitacaoCertidao:gravarNovaSolicitacao").click()}')

        if(instance == 2):
            driver.find_element(By.ID, 'formCriacaoSolicitacaoCertidao:tipoInstanciaCertidao:1').click()
            time.sleep(2)

        types = Select(driver.find_element(By.NAME, 'formCriacaoSolicitacaoCertidao:tipoCertidaoCertidao'))
        types.select_by_value(type)

        time.sleep(2)

        # Necessary because under this circunstance, the city id disabled, therefore, no need to fill it in.
        if(type != 'FINS_ELEITORAIS'):
            cities = Select(driver.find_element(By.NAME, 'formCriacaoSolicitacaoCertidao:comarcaCertidao2'))
            cities.select_by_visible_text(row['comarca'])

        if(row['tipopessoa'] == 'fisica'):
            driver.find_element(By.ID, 'formCriacaoSolicitacaoCertidao:tipoDocPesquisado:0').click()
            driver.find_element(By.NAME, 'formCriacaoSolicitacaoCertidao:cpfPesquisa').send_keys(row['cpfcnpj'])

        elif(row['tipopessoa'] == 'juridica'):
            driver.find_element(By.ID, 'formCriacaoSolicitacaoCertidao:tipoDocPesquisado:1').click()
            driver.find_element(By.ID, 'formCriacaoSolicitacaoCertidao:cnpjPesquisa').send_keys(row['cpfcnpj'])

        driver.find_element(By.NAME, 'formCriacaoSolicitacaoCertidao:nomePesquisado').send_keys(row['nome'])

        driver.find_element(By.NAME, 'formCriacaoSolicitacaoCertidao:nomeSolicitante').send_keys(row['nomesolicitante'])
        driver.find_element(By.NAME, 'formCriacaoSolicitacaoCertidao:cpfSolicitante').send_keys(row['cpfsolicitante'])
        driver.find_element(By.NAME, 'formCriacaoSolicitacaoCertidao:email').send_keys(row['e-mail'])
        driver.find_element(By.NAME, 'formCriacaoSolicitacaoCertidao:confirmacaoEmail').send_keys(row['e-mail'])

        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        driver.find_element(By.ID, 'captcha_text').send_keys('')

        WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.ID, 'voltarCertidao')))

        try:
            driver.implicitly_wait(0)
            driver.find_element(By.ID, "visualizarCertidao")
        except NoSuchElementException:
            logging.info('Certidao tipo %s nao disponivel para %s (%da instancia)' % (type, row['nome'], instance))
        finally:
            driver.implicitly_wait(30)


# Instanciate the class specifying the correct path to a valid CSV file
RupeReportsCrawler('rupe.csv')
