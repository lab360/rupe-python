# RUPE - Automatização

Este script tem como objetivo central a automação do processo de preenchimento das certidões públicas disponíveis, bem como o download das mesmas quando disponíveis.

## Dependências
- Python 3.5+
- Chrome (qualquer versão)
- ChromeDriver
- `pip` ou `conda`
- `selenium` (`pip install selenium`)

## Instruções
1. O script principal irá iniciar buscando por um arquivo no formato CSV, que contenha os dados estruturados tais quais os do arquivo de exemplo `rupe.csv`. É possível trocar o nome do arquivo alterando o primeiro parâmetro do instanciamento da classe.
1. Em seguida, ele irá abrir o sistema do RUPE e irá escanear os tipos de certidão disponíveis.
1. Daí em diante, irá preencher os dados e te direcionar ao preenchimento do captcha. Ao pressionar ENTER ele irá seguir com a execução e direcionar aos próximos relatórios, fazendo download dos mesmos sempre que disponíveis.
1. Os downloads serão salvos por padrão na pasta `files` e sairão com o mesmo nome dos originais.


## Documentação Auxiliar
- https://selenium-python.readthedocs.io
- http://chromedriver.chromium.org/getting-started
- https://docs.python.org/3/library/csv.html
